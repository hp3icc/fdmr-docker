#!/bin/bash
###menu
cat > /bin/menu-adn2 <<- "EOFX2"
#!/bin/bash
if [[ $EUID -ne 0 ]]; then
  whiptail --title "sudo su" --msgbox "Se requieren privilegios de superusuario. Ejecute 'sudo su' antes de ingresar al menú." 0 50
  exit 0
fi
while : ; do
  choix=$(whiptail --title "Scrip proyect HP3ICC FDMR+ Docker by CA5RPY" --menu "Suba o Baje con las flechas del teclado y seleccione el número de opción:" 22 75 13 \
1 " Edit ADN Server " \
2 " Edit FDMR-Monitor  " \
3 " Edit docker-Compose.yml  " \
4 " Start & Restart ADN Server  " \
5 " Stop ADN Server " \
6 " Info list Container Run  " \
7 " update " 3>&1 1>&2 2>&3)
exitstatus=$?
#on recupere ce choix
#exitstatus=$?
if [ $exitstatus = 0 ]; then
    echo "Your chosen option:" $choix
else
    echo "You chose cancel."; break;
fi
# case : action en fonction du choix
case $choix in
1)
nano /etc/ADN-Systems/adn.cfg ;;
2)
nano /etc/ADN-Systems/hbmon/fdmr-mon.cfg  ;;
3)
nano /etc/ADN-Systems/docker-compose.yml ;;
4)
start-fdmr ;;
5)
stop-fdmr ;;
6)
docker stats --all ;;
7)
bash -c "$(curl -fsSLk https://gitlab.com/hp3icc/fdmr-docker/-/raw/main/update.sh)";
esac
done
exit 0


EOFX2
##

ln -sf /bin/menu-adn2 /bin/MENU-ADN2
ln -sf /bin/menu-adn2 /bin/menu-fdmr2
ln -sf /bin/menu-adn2 /bin/MENU-FDMR2
sudo chmod +x /bin/menu*
sudo chmod +x /bin/MENU*

