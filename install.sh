#!/bin/bash

if [ $EUID -ne 0 ]; then
    whiptail --title "sudo su" --msgbox "requiere ser usuario root , escriba (sudo su) antes de entrar a menu / requires root user, type (sudo su) before entering menu" 0 50
    exit 0
fi
(crontab -l | grep -v "sync ; echo 3 > /proc/sys/vm/drop_caches >/dev/null 2>&1") | crontab -

######################################
#bash -c "$(curl -fsSLk https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/ipv6off.sh)" &&
bash -c "$(curl -fsSLk https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/docker.sh)"
# Buscar redes y eliminar si existen
for network in freedmr_app_net freedmr; do
  if docker network ls | grep -q "$network"; then
    echo "Eliminando red: $network"
    docker network rm "$network" 2>/dev/null
  fi
done
############################
if [ -f "/etc/freedmr/freedmr.cfg" ]
then
   cd /etc/freedmr/
   docker-compose down
   variableid=$(grep "SERVER_ID:" /etc/freedmr/freedmr.cfg | grep -Eo '[0-9]{1,9}')
fi
if [ -f "/etc/ADN-Systems/docker-compose.yml" ]
then
   cp /etc/ADN-Systems/docker-compose.yml /opt/docker-compose-bk.yml
fi
if [ -f "/etc/ADN-Systems/adn.cfg" ]
then
   cd /etc/ADN-Systems/
   docker-compose down
   variableid=$(grep "SERVER_ID:" /etc/ADN-Systems/adn.cfg | grep -Eo '[0-9]{1,9}')
fi
if [ -z "$variableid" ]
then variableid=0000

fi
if [ -d "/etc/ADN-Systems" ];
then
    cd /etc/ADN-Systems
  if docker ps --format '{{.Names}}' | grep -q '^adn-server$'; then
    # Detener y eliminar el contenedor utilizando Docker-Compose
      docker exec -i mariadb mysqldump -uhbmon -phbmon hbmon > /home/fdmr_mon.sql
      docker-compose down
      docker rmi freedmr
      docker rmi adn-server 
      docker rmi freedmr-monitor
      docker rmi freedmr-proxy
      docker rmi freedmr-freedmr 
      docker rmi freedmr-php-fpm 
      docker rmi willfarrell/autoheal
      docker rmi nginx 
      docker rmi lscr.io/linuxserver/mariadb
else
    echo "El contenedor freedmr no está en ejecución."
    cd /
  fi  
fi
#!/bin/bash

# Listar todas las imágenes de Docker
docker image ls

# Obtener los IMAGE ID de todas las imágenes listadas
image_ids=$(docker image ls -q)

# Iterar sobre los IMAGE ID y eliminar las imágenes una por una
for image_id in $image_ids
do
    docker image rm $image_id
done

######################################
bash -c "$(curl -fsSLk https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/rpiswap.sh)" &&
cd /root
echo FreeDMR Docker installer...
apps="sudo wget git ca-certificates curl gnupg lsb-release"

# Función para verificar e instalar una aplicación
check_and_install() {
    app=$1
    if ! dpkg -s $app 2>/dev/null | grep -q "Status: install ok installed"; then
        echo "$app no está instalado. Instalando..."
        sudo DEBIAN_FRONTEND=noninteractive apt-get install -y $app
        echo "$app instalado correctamente."
    else
        echo "$app ya está instalado."
    fi
}

# Verificar e instalar cada aplicación
for app in $apps; do
    check_and_install $app
done

if [ ! -f "/etc/apt/keyrings/docker.gpg" ];
then
    if [ ! -d "/etc/apt/keyrings" ];
    then
    rm -rf /etc/apt/keyrings     
    mkdir -p /etc/apt/keyrings
  fi
fi
#
if [ -d "/tmp/ADN-Systems" ];
then
    rm -r /tmp/ADN-Systems
fi
if [ -d "/etc/ADN-Systems" ];
then
    rm -r /etc/ADN-Systems
fi

#
echo "Make config directory..."
mkdir -p /etc/ADN-Systems
mkdir -p /etc/ADN-Systems/hbmon
mkdir -p /etc/ADN-Systems/acme.sh && 
mkdir -p /etc/ADN-Systems/certs &&
mkdir -p /etc/ADN-Systems/mysql/initdb.d
chmod -R 777 /etc/ADN-Systems

echo "make data directory..."
mkdir -p /etc/ADN-Systems/data
chown 54000:54000 /etc/ADN-Systems/data
chown 54000:54000 /etc/ADN-Systems/

echo "Install /etc/ADN-Systems/adn.cfg ..."

cat << EOF > /etc/ADN-Systems/adn.cfg
[GLOBAL]
PATH: ./
PING_TIME: 10
MAX_MISSED: 3
USE_ACL: True
REG_ACL: PERMIT:ALL
SUB_ACL: DENY:1
TGID_TS1_ACL: PERMIT:ALL
TGID_TS2_ACL: PERMIT:ALL
GEN_STAT_BRIDGES: True
ALLOW_NULL_PASSPHRASE: True
ANNOUNCEMENT_LANGUAGES:
SERVER_ID: 0000
DATA_GATEWAY: False
VALIDATE_SERVER_IDS: True

[REPORTS]
REPORT: True
REPORT_INTERVAL: 60
REPORT_PORT: 4321
REPORT_CLIENTS: *

[LOGGER]
LOG_FILE: /dev/null
LOG_HANDLERS: console-timed
LOG_LEVEL: DEBUG
LOG_NAME: FreeDMR

[ALIASES]
TRY_DOWNLOAD: True
PATH: ./data/
PEER_FILE: peer_ids.json
SUBSCRIBER_FILE: subscriber_ids.json
TGID_FILE: talkgroup_ids.json
PEER_URL: https://adn.systems/files/peer_ids.json
SUBSCRIBER_URL: https://adn.systems/files/subscriber_ids.json
TGID_URL: https://adn.systems/files/talkgroup_ids.json
LOCAL_SUBSCRIBER_FILE: subscriber_ids.json
STALE_DAYS: 1
SUB_MAP_FILE: sub_map.pkl
SERVER_ID_URL: https://adn.systems/files/server_ids.tsv
SERVER_ID_FILE: server_ids.tsv
CHECKSUM_URL: https://adn.systems/files/file_checksums.json
CHECKSUM_FILE: file_checksums.json
KEYS_FILE: keys.json

#Control server shared allstar instance via dial / AMI
[ALLSTAR]
ENABLED: False
USER:llcgi
PASS: mypass
SERVER: my.asl.server
PORT: 5038
NODE: 0000

[OBP-TEST]
MODE: OPENBRIDGE
ENABLED: False
IP:
PORT: 62044
NETWORK_ID: 1
PASSPHRASE: mypass
TARGET_IP: 
TARGET_PORT: 62044
USE_ACL: True
SUB_ACL: DENY:1
TGID_ACL: DENY:0-82,92-199,800-899,9990-9999,900999
RELAX_CHECKS: True
ENHANCED_OBP: True
PROTO_VER: 5

[SYSTEM]
MODE: MASTER
ENABLED: True
REPEAT: True
MAX_PEERS: 1
EXPORT_AMBE: False
IP: 
PORT: 56400
PASSPHRASE:
GROUP_HANGTIME: 5
USE_ACL: True
REG_ACL: DENY:1
SUB_ACL: DENY:1
TGID_TS1_ACL: PERMIT:ALL
TGID_TS2_ACL: PERMIT:ALL
DEFAULT_UA_TIMER: 60
SINGLE_MODE: False
VOICE_IDENT: False
TS1_STATIC:
TS2_STATIC:
DEFAULT_REFLECTOR: 0
ANNOUNCEMENT_LANGUAGE: es_ES
GENERATOR: 100
ALLOW_UNREG_ID: False
PROXY_CONTROL: False
OVERRIDE_IDENT_TG:

[ECHO]
MODE: MASTER
ENABLED: True
REPEAT: True
MAX_PEERS: 1
EXPORT_AMBE: False
IP: 127.0.0.1
PORT: 54917
PASSPHRASE:
GROUP_HANGTIME: 5
USE_ACL: True
REG_ACL: DENY:1
SUB_ACL: DENY:1
TGID_TS1_ACL: DENY:ALL
TGID_TS2_ACL: PERMIT:9990
DEFAULT_UA_TIMER: 1
SINGLE_MODE: True
VOICE_IDENT: False
TS1_STATIC:
TS2_STATIC:9990
DEFAULT_REFLECTOR: 0
ANNOUNCEMENT_LANGUAGE: en_GB
GENERATOR: 0
ALLOW_UNREG_ID: True
PROXY_CONTROL: False
OVERRIDE_IDENT_TG:

[D-APRS]
MODE: MASTER
ENABLED: True
REPEAT: False
MAX_PEERS: 1
EXPORT_AMBE: False
IP:
PORT: 52555
PASSPHRASE:
GROUP_HANGTIME: 0
USE_ACL: True
REG_ACL: DENY:1
SUB_ACL: DENY:1
TGID_TS1_ACL: PERMIT:ALL
TGID_TS2_ACL: PERMIT:ALL
DEFAULT_UA_TIMER: 10
SINGLE_MODE: False
VOICE_IDENT: False
TS1_STATIC:
TS2_STATIC:
DEFAULT_REFLECTOR: 0
ANNOUNCEMENT_LANGUAGE: es_ES
GENERATOR: 2
ALLOW_UNREG_ID: True
PROXY_CONTROL: False
OVERRIDE_IDENT_TG:



EOF
##
echo "Set perms on config directory..."
chown -R 54000 /etc/ADN-Systems

echo "Tune network stack..."
cat << EOF > /etc/sysctl.conf
net.core.rmem_default=134217728
net.core.rmem_max=134217728
net.core.wmem_max=134217728                       
net.core.rmem_default=134217728
net.core.netdev_max_backlog=250000
net.netfilter.nf_conntrack_udp_timeout=15
net.netfilter.nf_conntrack_udp_timeout_stream=35
EOF

/usr/sbin/sysctl -p

echo "Downloading ADN server..."

git clone https://gitlab.com/hp3icc/fdmr-docker.git /tmp/ADN-Systems
cp /tmp/ADN-Systems/docker-compose.yml /etc/ADN-Systems
cp -r /tmp/ADN-Systems/docker /etc/ADN-Systems
cp /etc/ADN-Systems/docker/mariadb/hbmon.sql /etc/ADN-Systems/mysql/initdb.d/
################
if [ "$(cat /proc/cpuinfo | grep 'Raspberry')" != "" ]; then
sed -i "s/cpu_shares: 1024/#cpu_shares: 1024/g"  /etc/ADN-Systems/docker-compose.yml
sed -i "s/mem_reservation: 600m/#mem_reservation: 600m/g"  /etc/ADN-Systems/docker-compose.yml
sed -i "s/adn-server-self-service:latest/adn-server-self-service:rpi/g"  /etc/ADN-Systems/docker-compose.yml
fi

#################

echo "Downloading hbmon..."
sed -i 's|git clone https://gitlab.com/hp3icc/FDMR-Monitor2.git|git clone https://gitlab.com/hp3icc/FDMR-Monitor.git|g' /etc/ADN-Systems/docker/monitor/Dockerfile
git clone https://gitlab.com/hp3icc/FDMR-Monitor.git /etc/ADN-Systems/hbmon
#git clone https://github.com/yuvelq/FDMR-Monitor.git /etc/ADN-Systems/hbmon
#git clone https://github.com/iu2naf/FDMR-Monitor.git /etc/ADN-Systems/hbmon
cd /etc/ADN-Systems/hbmon
git config --global --add safe.directory /etc/ADN-Systems/hbmon
git checkout Self_Service
chown -R 54000 /etc/ADN-Systems/hbmon/log
chmod 777 /etc/ADN-Systems/hbmon/data/

#wget -O /etc/ADN-Systems/hbmon/html/img/logo.png https://adn.systems/files/logo1.png 
wget -O /etc/ADN-Systems/hbmon/html/favicon.ico https://adn.systems/files/ADN_37x18_01.ico 

echo "Configuring..."

cp fdmr-mon_SAMPLE.cfg fdmr-mon.cfg

#wget https://raw.githubusercontent.com/CS8ABG/FDMR-Monitor/Self_Service/proxy/proxy_db.py -O /etc/ADN-Systems/hbmon/proxy/proxy_db.py
#sed -i "s/\/adn.cfg/\/config\/adn.cfg/g" /etc/ADN-Systems/hbmon/proxy/hotspot_proxy_v2.py
sed -i "s/db_selfcare =.*/db_selfcare = ProxyDB('localhost', 'hbmon', 'hbmon', 'hbmon', 3306)/g" /etc/ADN-Systems/hbmon/proxy/proxy_db.py
sed -i "s/54000/56400/g"  /etc/ADN-Systems/hbmon/proxy/proxy_db.py
sed -i "s/54100/56499/g"  /etc/ADN-Systems/hbmon/proxy/proxy_db.py

sed -i "s/FDMR_IP .*/FDMR_IP = 172.16.238.10/" fdmr-mon.cfg
sed -i "s/DB_SERVER .*/DB_SERVER = 172.16.238.11/" fdmr-mon.cfg
sed -i "s/LOG_PATH = .\/log/LOG_PATH = .\//" fdmr-mon.cfg
sed -i "s/LOG_LEVEL = INFO/LOG_LEVEL = DEBUG/" fdmr-mon.cfg

sed -i "s/SUBSCRIBER_FILE =.*/SUBSCRIBER_FILE = subscriber_ids.json/g" fdmr-mon.cfg
sed -i "s|SUBSCRIBER_URL.*|SUBSCRIBER_URL = https://adn.systems/files/subscriber_ids.json|g"  fdmr-mon.cfg

apt-get install rrdtool -y

sed -i 's/var\/www\/html/etc\/freedmr\/hbmon\/html/' /etc/ADN-Systems/hbmon/sysinfo/cpu.sh
sed -i 's/var\/www\/html/etc\/freedmr\/hbmon\/html/' /etc/ADN-Systems/hbmon/sysinfo/graph.sh
sed -i "s/opt\/FDMR-Monitor/etc\/freedmr\/hbmon/g"  /etc/ADN-Systems/hbmon/sysinfo/*.sh

chmod +x /etc/ADN-Systems/hbmon/sysinfo/cpu.sh
chmod +x /etc/ADN-Systems/hbmon/sysinfo/graph.sh
chmod +x /etc/ADN-Systems/hbmon/sysinfo/rrd-db.sh
##
sh /etc/ADN-Systems/hbmon/sysinfo/rrd-db.sh
(crontab -l; echo "*/5 * * * * sh /etc/ADN-Systems/hbmon/sysinfo/graph.sh")|awk '!x[$0]++'|crontab -
(crontab -l; echo "*/2 * * * * sh /etc/ADN-Systems/hbmon/sysinfo/cpu.sh")|awk '!x[$0]++'|crontab -
###
bash -c "$(curl -fsSLk https://gitlab.com/hp3icc/fdmr-docker/-/raw/main/custom.sh)"
#sed -i "s/TGID_URL = .*/TGID_URL = https:\/\/freedmr.cymru\/talkgroups\/talkgroup_ids_json.php/g" fdmr-mon.cfg
#sed -i "s/THEME_COLOR = .*/THEME_COLOR = pro/g" fdmr-mon.cfg
sed -i "s/FILES_PATH =.*/FILES_PATH = \/hbmon\/data/g" fdmr-mon.cfg
sed -i "s/RELOAD_TIME =.*/RELOAD_TIME = 1/g"  fdmr-mon.cfg
####

sed -i "s/path2config .*/path2config = \"\/hbmon\/fdmr-mon.cfg\";/" html/include/config.php

chmod -R 777 /etc/ADN-Systems/hbmon/log


######################################
chmod 777 /etc/ADN-Systems -R
###############################################
cat > /bin/start-fdmr <<- "EOF"
#!/bin/bash
rm /etc/ADN-Systems/hbmon/data/*
cd /etc/ADN-Systems
docker-compose down &&
docker-compose up -d
cronedit.sh '*/5 * * * *' 'sh /etc/ADN-Systems/hbmon/sysinfo/graph.sh' add
cronedit.sh '*/2 * * * *' 'sh /etc/ADN-Systems/hbmon/sysinfo/cpu.sh' add
cronedit.sh '* */24 * * *' 'rm /etc/ADN-Systems/hbmon/data/*' add


EOF
#
cat > /bin/stop-fdmr <<- "EOF"
#!/bin/bash
cd /etc/ADN-Systems
docker-compose down
cronedit.sh '*/5 * * * *' 'sh /etc/ADN-Systems/hbmon/sysinfo/graph.sh' remove
cronedit.sh '*/2 * * * *' 'sh /etc/ADN-Systems/hbmon/sysinfo/cpu.sh' remove
cronedit.sh '* */24 * * *' 'rm /etc/ADN-Systems/hbmon/data/*' remove

EOF
###############################################
cat > /usr/local/bin/cronedit.sh <<- "EOF"
cronjob_editor () {
# usage: cronjob_editor '<interval>' '<command>' <add|remove>

if [[ -z "$1" ]] ;then printf " no interval specified\n" ;fi
if [[ -z "$2" ]] ;then printf " no command specified\n" ;fi
if [[ -z "$3" ]] ;then printf " no action specified\n" ;fi

if [[ "$3" == add ]] ;then
    # add cronjob, no duplication:
    ( sudo crontab -l | grep -v -F -w "$2" ; echo "$1 $2" ) | sudo crontab -
elif [[ "$3" == remove ]] ;then
    # remove cronjob:
    ( sudo crontab -l | grep -v -F -w "$2" ) | sudo crontab -
fi
}
cronjob_editor "$1" "$2" "$3"


EOF
sudo chmod +x /usr/local/bin/cronedit.sh

(crontab -l | grep -v "rm /etc/ADN-Systems/hbmon/talkgroup_ids.json >/dev/null 2>&1") | crontab -
(crontab -l | grep -v "rm /etc/ADN-Systems/hbmon/user.json >/dev/null 2>&1") | crontab -
(crontab -l | grep -v "rm /opt/FDMR-Monitor2/data/*") | crontab -
(crontab -l | grep -v "rm /opt/FDMR-Monitor/data/*") | crontab -
(crontab -l | grep -v "rm /etc/ADN-Systems/hbmon/data/*") | crontab -
(crontab -l | grep -v "sh /etc/ADN-Systems/hbmon/sysinfo/cpu.sh") | crontab -
(crontab -l | grep -v "sh /etc/ADN-Systems/hbmon/sysinfo/graph.sh") | crontab -
(crontab -l | grep -v "sh /opt/FDMR-Monitor/sysinfo/cpu.sh") | crontab -
(crontab -l | grep -v "sh /opt/FDMR-Monitor/sysinfo/graph.sh") | crontab -
(crontab -l | grep -v "rm /etc/ADN-Systems/hbmon/talkgroup_ids.json >/dev/null 2>&1") | crontab -
(crontab -l | grep -v "rm /etc/ADN-Systems/hbmon/user.csv >/dev/null 2>&1") | crontab -
(crontab -l | grep -v "rm /etc/ADN-Systems/hbmon/user.json >/dev/null 2>&1") | crontab -

##########################
#!/bin/bash
cd /etc/ADN-Systems
# Comprobar si el contenedor MariaDB está en ejecución
if ! docker ps -f name=mariadb --format "{{.Names}}" | grep -q mariadb; then
  echo "El contenedor MariaDB no está en ejecución. Esperando a que se inicie..."

  # Iniciar el contenedor MariaDB
  docker-compose up -d mariadb

  # Esperar hasta que el contenedor esté en ejecución
#  while ! docker ps -f name=mariadb --format "{{.Names}}" | grep -q mariadb | grep -q Up; do
   while ! docker ps -f name=mariadb --format "{{.Names}} {{.Status}}" | grep -q "mariadb Up"; do

   sleep 1
  done
  echo "contenedor mariadb iniciado"
  
  while true; do
  if docker ps --format '{{.Status}}' | grep -q '^Up 30 seconds$'; then
    echo "mysql Ok"
    break
  else
      echo -ne "Por favor, espera [\033[1;33m-] \r"
      sleep 0.1
      echo -ne "Por favor, espera [\033[1;33m\] \r"
      sleep 0.1
      echo -ne "Por favor, espera [\033[1;33m|] \r"
      sleep 0.1
      echo -ne "Por favor, espera [\033[1;33m/] \r"
      sleep 0.1
  fi
  done
      if [ -f "/home/fdmr_mon.sql" ];
       then
    echo "restaurando base de datos"
    docker exec -i mariadb mysql -uhbmon -phbmon -S /run/mysqld/mysqld.sock hbmon < /home/fdmr_mon.sql &&
    docker exec -it mariadb mysql -u hbmon -phbmon -D hbmon -e "DROP TABLE IF EXISTS last_heard;" &&
    docker exec -it mariadb mysql -u hbmon -phbmon -D hbmon -e "DROP TABLE IF EXISTS lstheard_log;" &&
    docker exec -it mariadb mysql -u hbmon -phbmon -D hbmon -e "DROP TABLE IF EXISTS peer_ids;" &&
    docker exec -it mariadb mysql -u hbmon -phbmon -D hbmon -e "DROP TABLE IF EXISTS subscriber_ids;" &&
    docker exec -it mariadb mysql -u hbmon -phbmon -D hbmon -e "DROP TABLE IF EXISTS talkgroup_ids;" &&
    docker exec -it mariadb mysql -u hbmon -phbmon -D hbmon -e "DROP TABLE IF EXISTS tg_count;" &&
    docker exec -it mariadb mysql -u hbmon -phbmon -D hbmon -e "DROP TABLE IF EXISTS user_count;"
      fi
fi
#####################
# monitor
   
#!/bin/bash
cd /etc/ADN-Systems
# Comprobar si el contenedor MariaDB está en ejecución
if ! docker ps -f name=monitor --format "{{.Names}}" | grep -q monitor; then
  echo "El contenedor monitor no está en ejecución. Esperando a que se inicie..."

  # Iniciar el contenedor MariaDB
  docker-compose up -d monitor

  # Esperar hasta que el contenedor esté en ejecución
#  while ! docker ps -f name=mariadb --format "{{.Names}}" | grep -q mariadb | grep -q Up; do
   while ! docker ps -f name=monitor --format "{{.Names}} {{.Status}}" | grep -q "monitor Up"; do

   sleep 1
  done
  echo "contenedor mariadb iniciado"
  
  while true; do
  if docker ps --format '{{.Status}}' | grep -q '^Up 5 seconds$'; then
    echo "monitor Ok"
    break
  else
      echo -ne "Por favor, espera [\033[1;33m-] \r"
      sleep 0.1
      echo -ne "Por favor, espera [\033[1;33m\] \r"
      sleep 0.1
      echo -ne "Por favor, espera [\033[1;33m|] \r"
      sleep 0.1
      echo -ne "Por favor, espera [\033[1;33m/] \r"
      sleep 0.1
  fi
  done
      if docker ps -f name=monitor --format "{{.Names}} {{.Status}}" | grep -q "monitor Up"; then
    echo "Restaurando base de datos"
    docker exec -it monitor sh -c "cd /hbmon && python mon_db.py --create" &&
    docker exec -it monitor sh -c "cd /hbmon && python mon_db.py --update"
     fi

fi
(crontab -l; echo "* */24 * * * rm /etc/ADN-Systems/hbmon/data/*")|awk '!x[$0]++'|crontab -
(crontab -l; echo "*/5 * * * * sh /etc/ADN-Systems/hbmon/sysinfo/graph.sh")|awk '!x[$0]++'|crontab -
(crontab -l; echo "*/2 * * * * sh /etc/ADN-Systems/hbmon/sysinfo/cpu.sh")|awk '!x[$0]++'|crontab -

echo "Iniciando FreeDMR"
docker-compose down 
################################
#!/bin/bash

cd /etc/ADN-Systems
if [ -z "$variableid" ]
        then variableid=0000
fi
sudo sed -i "s/SERVER_ID: .*/SERVER_ID: $variableid/g"  /etc/ADN-Systems/adn.cfg
sudo cat /opt/obp.txt >> /etc/ADN-Systems/adn.cfg
sudo chmod +x /opt/extra-1d.sh
sh /opt/extra-1d.sh
if [ -f "/opt/docker-compose-bk.yml" ]
then
   cp /opt/docker-compose-bk.yml /etc/ADN-Systems/docker-compose.yml
fi
##########################
echo "Read notes in /etc/ADN-Systems/docker-compose.yml to understand how to implement extra functionality."
echo "FreeDMR setup complete!"

#############################################################
chmod +x /bin/menu*
chmod +x /bin/start-fdmr
chmod +x /bin/stop-fdmr
start-fdmr
history -c && history -w
#menu-fdmr2
bash -c "$(curl -fsSLk https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/ipv6on-custom.sh | sed 's/\r//g')"

