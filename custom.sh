#!/bin/bash
#sed -i '228s/20/50/' /etc/ADN-Systems/hbmon/proxy/hotspot_proxy_v2.py
#wget https://raw.githubusercontent.com/CS8ABG/FDMR-Monitor/Self_Service/proxy/hotspot_proxy_v2.py -O /etc/ADN-Systems/hbmon/proxy/hotspot_proxy_v2.py &&
wget https://gitlab.com/hp3icc/FDMR-Monitor2/-/raw/Self_Service/proxy/hotspot_proxy_v2.py -O /etc/ADN-Systems/hbmon/proxy/hotspot_proxy_v2.py &&
cat <<EOF > /etc/ADN-Systems/hbmon/proxy/proxy.cfg
[PROXY]
MASTER = 172.16.238.10
LISTENPORT = 62031
# Leave blank for IPv4, :: = all IPv4 and IPv6 (Dual Stack)
LISTENIP =
DESTPORTSTART = 56400
DESTPORTEND = 56499
TIMEOUT = 30
STATS = False
DEBUG = False
CLIENTINFO = True
BLACKLIST = [1234567,1231237,123123701]
#e.g. {10.0.0.1: 0, 10.0.0.2: 0}
IPBLACKLIST = {}

[SELF SERVICE]
USE_SELFSERVICE = True
SERVER = 172.16.238.11
USERNAME = hbmon
# For no password leave it blank
PASSWORD = hbmon
DB_NAME = hbmon
PORT = 3306
EOF

####   table
#
sudo cat > /etc/ADN-Systems/hbmon/html/buttons.php <<- "EOF"
<!-- HBMonitor buttons HTML code -->
<a class="button" href="index.php">Home</a>
&nbsp;
<div class="dropdown">
  <button class="dropbtn">Links</button>
  <div class="dropdown-content">
&nbsp;
<a class="button" href="linkedsys.php">Linked Systems</a>

<a class="button" href="statictg.php">Static TG</a>

<a class="button" href="opb.php">OpenBridge</a>
&nbsp;
</div>
</div>
<div class="dropdown">
  <button class="dropbtn">Self Service</button>
  <div class="dropdown-content">
    <?php if(!PRIVATE_NETWORK){echo '<a class="button" href="selfservice.php">SelfService</a>';}?>
    <a class="button" href="login.php">Login</a>
    <?php 
    if(isset($_SESSION["auth"], $_SESSION["callsign"], $_SESSION["h_psswd"]) and $_SESSION["auth"]){
      echo '<a class="button" href="devices.php">Devices</a>';
    }
    ?>
  </div>
</div>
<div class="dropdown">
  <button class="dropbtn">Local Server</button>
  <div class="dropdown-content">

<a class="button" href="moni.php">&nbsp;Monitor&nbsp;</a>

<a class="button" href="sysinfo.php">&nbsp;System Info&nbsp;</a>

<a class="button" href="log.php">&nbsp;Lastheard&nbsp;</a>

<a class="button" href="tgcount.php">&nbsp;TG Count&nbsp;</a>
&nbsp;
</div>
</div>
<div class="dropdown">
  <button class="dropbtn">ADN-Systems</button>
  <div class="dropdown-content">
&nbsp;
<a class="button" href="https://adn.systems/"target="_blank">&nbsp;Global Info ADN Systems;</a>
<a class="button" href="https://adn.systems/servers-list/"target="_blank">&nbsp;Info Server&nbsp;</a>
<a class="button" href="https://t.me/ADN_Systems"target="_blank">&nbsp;World Wide Telegram Groups&nbsp;</a>
&nbsp;
</div>
</div>

<!--
<a class="button" href="bridges.php">Bridges</a>
-->
<!-- Example of buttons dropdown HTML code -->
<!--
<div class="dropdown">
  <button class="dropbtn">Admin Area</button>
  <div class="dropdown-content">
    <a href="masters.php">Master&Peer</a>
    <a href="opb.php">OpenBridge</a>
    <a href="moni.php">Monitor</a>
  </div>
</div>
<div class="dropdown">
  <button class="dropbtn">Reflectors</button>
  <div class="dropdown-content">
    <a target='_blank' href="#">YSF Reflector</a>
    <a target='_blank' href="#">XLX950</a>
  </div>
</div>
-->
EOF

#

#logo
#wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?export=download&id=1evvxLOh8uxKYYLoV0aORjDhFeLF42_S_' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id=1evvxLOh8uxKYYLoV0aORjDhFeLF42_S_" -O /etc/ADN-Systems/hbmon/html/img/logo.png && rm -rf /tmp/cookies.txt &&
#favicon2.ico
#wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?export=download&id=1B-M7QNdf1gLVzbTn-Fi5GVPy6GTXcxJ-' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id=1B-M7QNdf1gLVzbTn-Fi5GVPy6GTXcxJ-" -O /etc/ADN-Systems/hbmon/html/favicon.ico && rm -rf /tmp/cookies.txt &&

sed '6 a <link rel="shortcut icon" href="/favicon.ico" />' -i /etc/ADN-Systems/hbmon/html/index.php
#sed -i "s/Copyright (c) 2016-.*/Copyright (c) <?php \$cdate=date(\"Y\"); if (\$cdate > \"2016\") {\$cdate=\"2016-\".date(\"Y\");} echo \$cdate; ?><br>/g" /etc/ADN-Systems/hbmon/html/*.php
#sed -i "s/meta name=\"description.*/meta name=\"description\" content=\"Copyright (c) 2016-22.The Regents of the K0USY Group. All rights reserved. Version OA4DOA 2022 (v270422)\">/g" /etc/ADN-Systems/hbmon/html/*.php
#sed -i "s/All rights reserved.<br>.*/All rights reserved.<br>, <a href=https:\/\/gitlab.hacknix.net\/hacknix\/FreeDMR\/-\/wikis\/Home>FreeDMR<\/a> by <a href=\"http:\/\/www.qrz.com\/db\/G7RZU\"target=\"_blank\">G7RZU hacknix<\/a>, Script project: <a title=\"Raspbian Proyect by HP3ICC © <?php \$cdate=date(\"Y\"); if (\$cdate > \"2018\") {\$cdate=\"2018-\".date(\"Y\");} echo \$cdate; ?>\" target=\"_blank\" href=https:\/\/gitlab.com\/hp3icc\/Easy-FreeDMR-Docker\/>FDMR+ Docker<\/a><br>/g" /etc/ADN-Systems/hbmon/html/*.php
##
###############################
if [ -f "/opt/extra-1d.sh" ]
then
  echo "found file"
else
  sudo cat > /opt/extra-1d.sh <<- "EOF"
######################################################################
# Coloque en este archivo, cualquier instruccion shell adicional que # 
# quierre se realice al finalizar la actualizacion.                  #
######################################################################
 
  
EOF
fi
if [ -f "/opt/obp.txt" ]
then
   echo "found file"
else
  sudo cat > /opt/obp.txt <<- "EOF"
 
#Coloque abajo su lista de obp y peer
EOF
fi
#
(crontab -l; echo "* */24 * * * rm /etc/ADN-Systems/hbmon/data/*")|awk '!x[$0]++'|crontab -

#####
#menu
bash -c "$(curl -fsSLk https://gitlab.com/hp3icc/fdmr-docker/-/raw/main/menu-adn2.sh)";
##


